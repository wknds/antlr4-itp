#!/bin/dash

# ----- Argument handling  -----------------

usage() {
	echo "Usage: $(basename ${0}) [-t \"CSharp\"] [-v] [-p <package_name>] [<output directory>]"
}

output_folder=.
target_lang=""
package_name=""
visitor_param=""
while getopts ':t:p:vh' opt; do
    	case $opt in
      		(t) target_lang=$OPTARG;;
		(p) package_name=$OPTARG;;
		(v) visitor_param="-visitor";;
		(h) usage; exit 0;;
		(:) echo $OPTARG
		    case $OPTARG in
			(t) usage; exit 2;;
			(v) usage; exit 2;;
            	    esac;;
    	esac
done

shift $(($OPTIND - 1))

if [ ! -z $1 ] # check path
then
	ls $1 2> /dev/null 1>&2;
	[ $? -gt 0 ] && echo "output directory '${1}' not found" && exit 2 ;
	output_folder=$(realpath ${1})
fi

if [ "${target_lang}" = "CSharp" ] 
then
	target_lang="-Dlanguage=${target_lang}"
else
	target_lang=""
fi

if ! [ -z "${package_name}" ]
then
	package_name="-package ${package_name}"
fi

# ----- setup paths and environment -----------

script_folder=$(realpath $(dirname $0))
grammar_folder=$(realpath $(dirname ${script_folder}/../antlr4-grammar/.)) || exit 2;

. ${script_folder}/antlr4_setenv.sh

# ----- generation ------------------------------

if [ "$(realpath $(dirname ${output_folder}))" = "${grammar_folder}" ]
then
	echo "warning: cannot clean old files in output directory, since it contains grammar files"
else
	printf "Clean old files..."
	rm *.interp *.java *.tokens *.class *.cs 2> /dev/null 1>&2
	echo "[ok]";
fi

printf "Generate target codes..." && antlr4 -o $output_folder $target_lang $visitor_param $package_name ${grammar_folder}/*.g4 && echo "[ok]" 
