#!/bin/dash

# ----- Argument handling  -----------------

usage() {
	echo "Usage: $(basename ${0}) GRAMMAR_NAME START_POINT TESTFILE"
}

[ $# -lt 3 ] && usage && exit 2;

# ----- setup paths and environment -----------)
script_folder=$(realpath $(dirname $0))
. ${script_folder}/antlr4_setenv.sh
test_file=$(realpath ${3})

# ----- generation ------------------------------

$(sh ${script_folder}/antlr4_generate.sh /tmp/)
javac /tmp/*.java 
printf "Zeige Parsetree..." && $(cd /tmp/ && grun ${1} ${2} ${test_file} -gui && echo "")
