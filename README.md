# antlr4-itp

ANTLR4 Grammatik für die [ITP Language](https://docshield.kofax.com/CCM/en_US/500-hxhwj84j0u/print/CCMTemplatescriptinglanguage.pdf). Die Grammatik ist noch nicht vollständig.

Erzeugen des Parsers mit Listenerklassen und Visitorklassen (-v) in C# nach zielordner: 
```
$ ./script/antlr4_generate.sh -vt "CSharp" <zielordner>
```
