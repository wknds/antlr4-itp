lexer grammar ItpLexer;
tokens {STRING}
// ---- BEGIN / END ------------

BEGIN_KEY : '#BEGIN' ;
END_KEY : 'END#' ;
INC : '__INC' ;

// ---- Scopes -----------------
DO : 'DO' ;
OD : 'OD' ;

// ---- Types -------------------

TYPE_NUMBER : 'NUMBER' ;
TYPE_TEXT : 'TEXT' ;
TYPE_BOOL : 'BOOL' ;
TYPE_CONST : 'CONST' ;
TYPE_ARRAY : 'ARRAY' ;
TYPE_MAP : 'MAP' ;

NUMBER : LiteralNumber ;
BOOL : LiteralBool ;
DQUOTE : '"' ~["]* '"' -> type(STRING);

// ----- Declarations -----------
FUNC : 'FUNC';

// ----- Assignments -------------

ASSIGN_KEY : 'ASSIGN' ;
ASSIGN_OPERATOR : ':=' ;

// ----- Function calls -----------

PAR_OPEN : ParenthesisOpen;
PAR_CLOSE : ParenthesisClose;
ARG_SEPARATOR : ArgumentSeparator;

// ----- IF-ELIF Statement -------------
IF : 'IF';
FI: 'FI';
THEN : 'THEN';
ELIF : 'ELIF';
ELSE : 'ELSE';
EQUALS : '=';
NOT_EQUALS : '<>';

AND : 'AND';
OR : 'OR';

CONCAT_SIGN : '+';

WS : [ \t\r\n]+ -> channel(1) ;
ID : Id ;
DOT: '.';
DECL_PARAM : FunctionArgumentChars ; 
COMMENT : '(*' .*? '*)' -> skip ;

TXTMODE_OPEN : '#' -> pushMode(TXTMODE_INSIDE); 
INCMODE_OPEN : INC PAR_OPEN -> pushMode(INCMODE_INSIDE);

mode INCMODE_INSIDE;
INC_TEXT : (~[()])+;
INCMODE_CLOSE : ')' -> popMode ;

mode TXTMODE_INSIDE;
TEXT : (~[/#@] | TXT_ESC)+;
TXT_ESC : '/#' | '//' | '/@';
TXTMODE_INSTR : '@(' TEXT ')';
TXTMODE_CLOSE : '#' -> popMode ;

fragment Id : [a-zA-Z0-9_]+ ;
fragment FunctionArgumentChars : [a-z0-9_]+;
fragment ArgumentSeparator : ';' ;
fragment LiteralBool : 'TRUE' | 'FALSE';
fragment LiteralNumber : [0-9]+([.][0-9]+)? ;
fragment ParenthesisOpen : '(' ;
fragment ParenthesisClose : ')' ;
