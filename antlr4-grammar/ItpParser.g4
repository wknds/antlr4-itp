parser grammar ItpParser;
options { tokenVocab=ItpLexer; }

start 	: BEGIN_KEY block END_KEY;

block 	: (instrMode | txtMode | include)*? ;
instrMode : (stat | decl)+ ;
//include : INC PAR_OPEN .+? PAR_CLOSE;
include: INCMODE_OPEN INC_TEXT+ INCMODE_CLOSE;
txtMode  : TXTMODE_OPEN (TEXT|TXTMODE_INSTR)+ TXTMODE_CLOSE;

// ------ Statements --------------------------

stat 		: assignment 
		| funcCall 
		| ifStat
		;
doStat 	: DO block OD ;

// ------ Declarations ------------------------

decl 	: variableDecl 
	| funcDecl
	;

// ------ Variable decl ----------------

variableDecl : type ID (ASSIGN_OPERATOR value)?;
type 	: TYPE_NUMBER
	| TYPE_TEXT 
	| TYPE_BOOL
	;

// ------ Function decl ------------------

funcDecl : FUNC type ID PAR_OPEN declParams PAR_CLOSE doStat;
declParams : declParam? (ARG_SEPARATOR declParam)*? ;
declParam : typeStructure? type DECL_PARAM;
typeStructure: (TYPE_CONST | TYPE_ARRAY | TYPE_MAP);

// ------ Assignments --------------------------

assignment : ASSIGN_KEY variableName ASSIGN_OPERATOR ((variableName CONCAT_SIGN)*? value (CONCAT_SIGN variableName)*? | variableName | funcCall) ;

// ------ Function call ----------------------

funcCall : ID PAR_OPEN exprList PAR_CLOSE ;

// ------ Expressions -----------------------

exprList : expr? (ARG_SEPARATOR expr)*? ;
expr 	: value
	| variableName
	| funcCall
	;
variableName : ID (DOT ID)*?;

// ------ IF-ELIF-Statement ------------------
ifStat : IF conditions THEN block (ELIF conditions THEN block)*? (ELSE block)? FI;
conditions : condition ((AND|OR) conditions)*? | PAR_OPEN conditions PAR_CLOSE ((AND|OR) conditions )*?;
condition : BOOL | (variableName condOp value);
condOp 	: EQUALS 
	| NOT_EQUALS
	;

// ------ Literal value --------------------

value : NUMBER | BOOL | STRING;
